/**
 * @author Daniel Cerhak & Jonas Weigelt <daniel.cerhak@fh-erfurt.de & jonas.weigelt@fh-erfurt.de>
 * @version 1.0.0
 * @description Grunt runner file, compile less, js and other stuff
 */

const { getRounds } = require('bcrypt');

module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            development: {
                options: {
                    paths: [
                        'src/less'
                    ],
                    compress: true,
                    plugins: [
                        new (require('less-plugin-autoprefix'))({
                            browsers: [
                                'last 2 versions',
                                'ie 9'
                            ]
                        })
                    ],
                    banner:
                        '/*!\n' +
                        ' * Created by Daniel Cerhak & Jonas Weigelt - Your specialists for Web-Development. Project: <%= pkg.name %> <%= grunt.template.today("yyyy.mm.dd")\n' +
                        ' * @author Daniel Cerhak & Jonas Weigelt \n' +
                        '*/\n',
                },
                files: {
                    'assets/css/layout.css': 'src/less/layout.less',
                    'assets/css/index.css': 'src/less/index.less',
                    'assets/css/signin.css': 'src/less/signin.less',
                    'assets/css/projects.css': 'src/less/projects.less',
                    'assets/css/imprint.css': 'src/less/imprint.less',
                    'assets/css/privacy.css': 'src/less/privacy.less',
                }
            }
        },
        uglify: {
            build: {
                files: {
                    'assets/js/_slideInPopover.min.js': 'src/js/_slideInPopover.js',
                    'assets/js/index.min.js': 'src/js/index.js',
                    'assets/js/layout.min.js': 'src/js/layout.js',
                    'assets/js/signin.min.js': 'src/js/signin.js',
                    'assets/js/projects.min.js': 'src/js/projects.js',
                }
            }
        },
        copy: {
            main: {
                files: [
                    { src: 'node_modules/html5sortable/dist/html5sortable.min.js', dest: 'assets/js/html5sortable.min.js'},
                ],
            },
        },
        watch: {
            scripts: {
                files: [
                    'src/less/**',
                    'src/js/**'
                ],
                tasks: ['less', 'uglify'],

            }
        }

    });

    grunt.registerTask('leak', 'Simple Leak Task', function () {
        grunt.log.writeln(this.name + ' is running.');
    });

    // load plugins
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    grunt.loadNpmTasks('grunt-contrib-copy');


    //register task
    grunt.registerTask('build', ['less', 'uglify', 'copy']);
    grunt.registerTask('default', ['watch']);
}