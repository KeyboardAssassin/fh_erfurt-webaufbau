const Passport = require('../core/passport.js');

module.exports = function (Model) {
    Model.prototype.fullname = function () {
        return this.firstName + ' ' + this.lastName;
    };

    Model.prototype.shortName = function () {
        return this.firstName.charAt(0) + this.lastName.charAt(0);
    };


    // Function for adding remote data to self
    Model.prototype.setAttributes = function (data, attributes) {
        const self = this;

        attributes.forEach(cur => {
            if (typeof data[cur] !== 'undefined') {
                self[cur] = data[cur];
            }
        });
    };

    Model.prototype.writeRemotes = function (data) {
        const self = this;


        if (typeof data.email === 'string') {
            self.email = data.email.toLowerCase();
        }
        else if (typeof data.email !== 'undefined') {
            data.email = data.email;
        }

        self.setAttributes(data, ['firstName', 'lastName'])

        if (typeof data.password !== 'undefined') {
            self.passwordHash = Passport.hashPassword(data.password);
        }
    }
};