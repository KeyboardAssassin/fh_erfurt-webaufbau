module.exports = function (Model, db) {
    Model.extendInclude = [
        {
            model: db.User,
            as: 'from',
            attributes: ['id', 'firstName', 'lastName'],
            limit: 1,
            separate: false
        },
        {
            model: db.User,
            as: 'to',
            attributes: ['id', 'firstName', 'lastName'],
            limit: 1,
            separate: false
        }
    ]

    Model.prototype.setAttributes = function (data, attributes) {
        const self = this;

        attributes.forEach(cur => {
            if (typeof data[cur] !== 'undefined') {
                self[cur] = data[cur];
            }
        });
    };

    Model.prototype.writeRemotes = function (data) {
        const self = this;

        // write remote data to self
        self.setAttributes(data, ['text', 'fromId', 'toId']);

    }
}