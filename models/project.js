module.exports = function (Model, db) {
    Model.extendInclude = [
        {
            model: db.Task,
            as: 'tasks',
            attributes: ['id', 'name', 'text'],
            separate: false,
            limit: 1
        }
    ];


    Model.prototype.setAttributes = function (data, attributes) {
        const self = this;

        attributes.forEach(cur => {
            if (typeof data[cur] !== 'undefined') {
                self[cur] = data[cur];
            }
        });
    };

    Model.prototype.writeRemotes = function (data) {
        const self = this;

        // write remote data to self
        self.setAttributes(data, ['name']);

    }
}

