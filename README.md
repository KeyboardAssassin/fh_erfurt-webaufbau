# Webaufbau - Taskboard

Hier entsteht das Taskboard Projekt von Daniel Cerhak und Jonas Weigelt.
Das Projekt ist Teil des Moduls Webaufbau des SS2020.

***Die finale Version des Projektes hier auf GitLab zu finden: [https://gitlab.com/KeyboardAssassin/fh_erfurt-webaufbau](https://gitlab.com/KeyboardAssassin/fh_erfurt-webaufbau)***

***Die live Version des Projektes ist auf Heroku zu finden: [https://wa-jwdc-taskboard.herokuapp.com](https://wa-jwdc-taskboard.herokuapp.com)***


## Dokumentation

Die folgenden Pfade führen zu allen notwendigen Dokumentationen:

* Allgemeine Dokumentation: [doc/Allgemeine-Dokumentation/](doc/Allgemeine-Dokumentation/Gesamtdokumentation.pdf)
* API-Dokumentation: [doc/Api-Dokumentation](doc/Api-Dokumentation/api-documentation.pdf)
* Styling-Dokumentation: [doc/Styling-Dokumentation/](doc/Styling-Dokumentation/styling-documentation.pdf)

## Entwickler Informationen

Git-Repository-URL zum Klonen:
* https://gitlab.com/KeyboardAssassin/fh_erfurt-webaufbau.git

Voraussetzungen für die Entwicklung:
* [Git](./doc/Externe-Dokumentation/Git.md)
* [Node.js](https://nodejs.org/docs/latest-v13.x/api/)
* [MySQL](https://dev.mysql.com/doc/)
* [Grunt](./doc/Externe-Dokumentation/Grunt.md)
* [Nodemon](./doc/Externe-Dokumentation/Nodemon.md)
* [Sequelize](./doc/Externe-Dokumentation/Sequelize.md)
* [Express](./doc/Externe-Dokumentation/Express.md)
* [Bcrypt](./doc/Externe-Dokumentation/Bcrypt.md)
* [Heroku](https://devcenter.heroku.com/categories/reference)

POSTMAN um eine Kollektion von HTTP Requests zu definieren (REST-Schnittstelle) 

## Installationsanleitung

1. Projekt entpacken
2. Verzeichnis in VSCode öffnen
3. "npx install" um alle notwendigen Pakete zu installieren
4. "npx sequelize-cli db:migrate:all" um die Datenbank zu migrieren
5. "npx sequelize-cli db:seed:all" um die Seeds in die Datenbank zu schreiben
6. "npx nodemon .\index.js" zum starten der Index

optional (nicht notwendig):

- weiteres Terminal öffnen
- "npx grunt build"
- "npx grunt watch" zur Überwachung und Kompilierung von less/uglify

## Nutzer Daten

ID: kristof.friess@fh-erfurt.de
PW: 12345678

ID: rolf.kruse@fh-erfurt.de
PW: 12345678
