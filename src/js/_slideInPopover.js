function SlideInPopover(opts)
{
    let self = this;

    opts = Object.assign({
        id: 'slide-in-popover',


    }, opts);
    console.log('opts',opts);

    let _shown = false; // stores the current display state
    let _editMode = false; // stores the current edit mode active or not
    let _task = null; // current selected task

    let _dom = null;
    let _elmBackground = null;
    let _elmTopBar = null;
    let _elmBottomBar = null;
    let _elmTitle = null;
    let _elmDescription = null;
    let _elmCreatedBy = null;
    let _elmAssignedTo = null;
    let _elmDeadline = null;
    let _elmDeadlineLabel = null;
    let _elmDeadlineShow = null;
    let _elmDeadlineChange = null;
    let _elmWorkflowStatus = null;
    let _elmEditButton = null;

    function init()
    {
        _dom = document.getElementById(opts.id);
        console.log('_dom',_dom);

        _contentAddTask = _dom.querySelector('.content-add-task');
        _contentShowTask = _dom.querySelector('.content-show-task');


        _elmBackground = _dom.querySelector('.background');
        _elmTopBar = _dom.querySelector('.top-bar');
        _elmBottomBar = _dom.querySelector('.bottom-bar');
        _elmTitle = _dom.querySelector('.task-title');
        _elmDescription = _dom.querySelector('.description');
        _elmCreatedBy = _dom.querySelector('.created-by');
        _elmAssignedTo = _dom.querySelector('.assigned-to');
        _elmDeadline = _dom.querySelector('.deadline');
        _elmDeadlineLabel = document.getElementById('deadline-label');
        _elmDeadlineShow = _dom.querySelector('.show-deadline');
        _elmDeadlineChange = _dom.querySelector('.change-deadline');
        _elmDeadlineChangeInput = document.getElementById('deadline-change');
        _elmDeadlineAddInput = document.getElementById('deadline-add');
        _elmWorkflowStatus = _dom.querySelector('.workflow-status');
        _elmEditButton = _dom.querySelector('.edit-btn');


        _elmDescriptionAdd = _dom.querySelector('.description-add');
        _elmAssignedToAdd = _dom.querySelector('.assigned-to-add');
        _elmAssignedToChange = _dom.querySelector('.assigned-to-change');


        _elmBackground.addEventListener('click', () => {
            self.hide();
        });

        _elmBackground.addEventListener('touch', () => {
            self.hide();
        });

        _elmEditButton.addEventListener('click', () => {

            self.editMode(!_editMode);

            if(_editMode === true){
                _elmEditButton.innerText = 'Save Task';
        
            }
            else
            {
                _elmEditButton.innertext = 'Edit Task';
            }
        });
    }

    function createSCEditor(textarea)
    {
        sceditor.create(textarea, {
            format: 'bbcode',
            toolbar: 'bold,italic,underline|source',
            style: '/assets/libs/sceditor/minified/themes/content/default.min.css',
            emoticonsEnabled: false
        });
    }

    
        // public methods
        self.showWithTaskId = function(taskId)
        {
            _contentAddTask.style.display ="none";
            _contentShowTask.style.display ="block";


            let xhr = new XMLHttpRequest();
            
            // handle request finished
            xhr.onload = function() {
                if (xhr.status >= 200 && xhr.status < 300) {
                    let jsonData = JSON.parse(xhr.response);
                    
                    _task = jsonData.task;

                    _elmTopBar.innerText = _elmTopBar.getAttribute('data-tpl').replace('{{id}}', _task.id);
                    _elmTitle.innerText = _task.name;
                    _elmCreatedBy.innerText = _task.creator.displayName;
                    _elmAssignedTo.innerText = _task.assignedTo.displayName;
                    _elmWorkflowStatus.innerText = _task.workflow.name;

                    let dateNow = Date.now();
                    deadline = Date.parse(_task.deadline);

                    let delta = 0;

                    if(dateNow < deadline)
                    {
                        _elmDeadlineLabel.innerText = 'Expires in'
                        delta = Math.abs(deadline - dateNow) / 1000;
                    }
                    else
                    {
                        _elmDeadlineLabel.innerText = 'Expired since'
                        delta = Math.abs(dateNow - deadline) / 1000;
                        _elmDeadline.classList.add('overdue')
                    }

                    console.log('_task.deadline', deadline);
                    console.log('dateNow', dateNow);
                    console.log('delta', delta);

                    // calculate (and subtract) whole days
                    let days = Math.floor(delta / 86400);
                    delta -= days * 86400;

                    // calculate (and subtract) whole hours
                    let hours = Math.floor(delta / 3600) % 24;
                    delta -= hours * 3600;

                    // calculate (and subtract) whole minutes
                    let minutes = Math.floor(delta / 60) % 60;
                    delta -= minutes * 60;

                    _elmDeadline.innerText = `${days}d ${hours}h ${minutes}m`;

                    let textarea = document.createElement('textarea');
                    textarea.innerHTML = _task.text;
                    _elmDescription.appendChild(textarea);

                    createSCEditor(textarea);
                    let instance = sceditor.instance(textarea);
                    _elmDescription.innerHTML = instance.fromBBCode(instance.val());

                    _dom.setAttribute('data-open', '1');
                    _shown = true;
                }
                else {
                    console.log('request failed');
                }
            };

            let url = '/api/tasks/' + taskId;

            xhr.open('GET', url);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.send();
        };
        
        self.showAddTask = function()
        {
            _contentAddTask.style.display ="block";
            _contentShowTask.style.display ="none";

            _dom.setAttribute('data-open', '1');
            
            let textarea = document.createElement('textarea');
            textarea.innerHTML = '';
            textarea.style.width = '100%';
            textarea.style.height = '420px';

            _elmDescriptionAdd.innerHTML = '';
            _elmDescriptionAdd.appendChild(textarea);
            createSCEditor(textarea);

            _elmDescriptionAdd.waTXT = textarea;

        };

        self.createTask = function()
        {
            let task = {};
            
            let workflowId = _contentAddTask.getAttribute("workflow-id");

            let instance = sceditor.instance(_elmDescriptionAdd.waTXT); 
            task.name = document.getElementById('title-add').value;
            task.text = instance.val();
            task.assignedToId = _elmAssignedToAdd.options[_elmAssignedToAdd.selectedIndex].getAttribute('assigned-to-id');
            task.workflowId = workflowId;
            task.projectId = document.getElementById('main-wrapper').getAttribute('project-id');
            task.deadline = _elmDeadlineAddInput.value;

            io.emit('task/create', task);
            
            _elmDescription.waTXT = null;



        }


        self.hide = function()
        {
            _dom.setAttribute('data-open', '0');
            _shown = false;
        }
        
        self.editMode = function(editMode)
        {
            if(editMode === true && _editMode === false)
            {
                _editMode = true;

                console.log('_elmAssignedToChange',_elmAssignedToChange);
                _elmAssignedToChange.style.display = 'block';
                _elmAssignedTo.style.display = 'none';

                _elmDeadlineShow.style.display = 'none'
                _elmDeadlineChange.style.display = 'block';

                let textarea = document.createElement('textarea');
                textarea.innerHTML = _task.text;
                textarea.style.width = '100%';
                textarea.style.height = '420px';

                _elmDescription.innerHTML = '';
                _elmDescription.appendChild(textarea);
                createSCEditor(textarea);

                _elmDescription.waTXT = textarea;

            }
            else if(_editMode === true)
            {
                _editMode = false;

                _elmAssignedToChange.style.display = 'none';
                _elmAssignedTo.style.display = 'block';
                
                _elmDeadlineShow.style.display = 'block'
                _elmDeadlineChange.style.display = 'none';

                _task.deadline = _elmDeadlineChangeInput.value;

                let instance = sceditor.instance(_elmDescription.waTXT); 
                _task.text = instance.val();
                _task.assignedToId = _elmAssignedToChange.options[_elmAssignedToChange.selectedIndex].getAttribute('assigned-to-id');
                _elmDescription.innerHTML = instance.fromBBCode(instance.val());
                _elmDescription.waTXT = null;




                self.updateTask(_task);
            }
        }

        self.updateTask = function(task) {
            let xhr = new XMLHttpRequest();
            
            // handle request finished
            xhr.onload = function() {
                if (xhr.status >= 200 && xhr.status < 300) {
                    _elmEditButton.innerHTML = 'Edit Task';
                }
                else {
                    console.log('request failed');
                }
            };

            console.log('task.id',task.id);
            let url = '/api/tasks/' + task.id;

            task.assignedTo = task.assignedToId

            xhr.open('PUT', url);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.send(JSON.stringify({task: task}));
           
        }

    // start init
    init();
}