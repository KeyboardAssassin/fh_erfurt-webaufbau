
window.onload = function () {
    list = document.querySelectorAll(".chat-button");
    for (var i = 0; i < list.length; i++) {
        list[i].addEventListener("click", function (e) {
            let wrapper = document.getElementById('main-wrapper');
            let chatOpened = wrapper.getAttribute('chat-open');

            if (!chatOpened) {
                wrapper.classList.add("chat-open");
            }
        });
    }


    console.log('list', list);

    let closeBtn = document.getElementById('close-btn');
    let switchBtn = document.getElementById('switch-project-btn');
    let logoutBtn = document.getElementById('logout-btn');

    closeBtn.addEventListener("click", function (e) {
        let wrapper = document.getElementById('main-wrapper');

        wrapper.classList.remove('chat-open');
    })

    switchBtn.addEventListener("click", function (e) {
        window.location = '/';
    })

    logoutBtn.addEventListener("click", function (e) {
        delete_cookie();
        window.location = '/';
    })


    tasks = document.querySelectorAll('.task');



}

//default chat is all
let textarea = document.getElementById('message');
let currentUserElm = document.getElementById('btn-chat-all');
let messagesElm = document.getElementById('messages');
let chatTitleElm = document.getElementById('chat-title');

document.addEventListener('DOMContentLoaded', function () {
    /// global variables
    let chatOpen = false;
    {
        // check local storage chat keep open last time?
        let storedChatState = localStorage.getItem('chatOpen');
        if (storedChatState === '1') {
            chatOpen = true;
            let mainWrapper = document.getElementById('main-wrapper');
            if (mainWrapper) {
                mainWrapper.className = 'wrapper chat-open';
            }
        }
    }

    // handle task show/create and edit popover
    let slideInPopover = new SlideInPopover({});

    // retrieve all task rendered by ejs (backend)
    let tasks = document.querySelectorAll('.task');

    for (let index = 0; index < tasks.length; index++) {
        const task = tasks[index];
        task.addEventListener('click', function (e) {
            slideInPopover.showWithTaskId(this.getAttribute('data-id'));
        });
    }


    //adding tasks
    showAddBtns = document.getElementsByClassName("add");
    for (var i = 0; i < showAddBtns.length; i++) {
        showAddBtns[i].addEventListener("click", function (e) {
            const self = this;
            let workflowId = self.getAttribute('workflow-id');

            addTaskWrapper = document.querySelector('.content-add-task');
            addTaskWrapper.setAttribute("workflow-id", workflowId);

            slideInPopover.showAddTask();
        });
    }

    createTaskBtn = document.querySelector(".create-task-btn");
    createTaskBtn.addEventListener("click", function (e) {
        slideInPopover.createTask();
    })
});

io.on('task/create', (data) => {
    handleNewTask(data);
});

function handleNewTask(task) {
    let workflows = document.querySelectorAll('.tasks');

    let elm = document.createElement('DIV');

    elm.innerHTML = `<div class="line" style="background: ${task.workflowcolor};"></div><div class="name">#${task.id} ${task.name}</div><div class="time">123:00 / 240:00 <button>play/pause</button></div><div class="footer"><div class="assigned">${task.assignedTo}</div><div class="deadline"></div></div>`;

    elm.classList.add("task");
    elm.setAttribute('data-id', task.id);

    for(index = 0; index < workflows.length; index++)
    {
        htmlWorkflowId = workflows[index].getAttribute('data-workflow-id')
        console.log('task.workflowId', task.workflowcolor);

        if(htmlWorkflowId != task.workflow)
        {
            continue;
        }
        else
        {
            workflows[index].appendChild(elm);
        }
    }
}


/////////////////////////////////////////////////////
/////////////////// Chat Handling ///////////////////
/////////////////////////////////////////////////////




function loadMessages(elm, fromId) {
    let xhr = new XMLHttpRequest();

    // handle request finished
    xhr.onload = function () {
        if (xhr.status >= 200 && xhr.status < 300) {
            let jsonData = JSON.parse(xhr.response);

            // map messages into socket messages format
            // and write to the button
            if (elm.lastMessages instanceof Array === false) {
                elm.lastMessages = [];
            }

            //append to top
            for (let index = 0; index < jsonData.messages.length; index++) {
                const message = jsonData.messages[index];
                elm.lastMessages.unshift({
                    text: message.text,
                    from: {
                        id: message.from.id,
                        displayName: message.from.firstName + ' ' + message.from.lastName
                    },
                    to: message.to
                });
            }

            userPressed(elm);
        }
        else {
            console.log('request failed');
        }
    };

    let url = messagesElm.getAttribute('data-action');

    if (fromId) {
        url += '?fromId=' + fromId;
    }

    if (elm.lastMessages instanceof Array === true) {
        if (url.indexOf('?') === -1) {
            url += '?';
        }
        else {
            url += '&';
        }

        url += 'offset=' + elm.lastMessages.length;
    }

    xhr.open('GET', url);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send();
};

function updateUserBtnName(elm) {
    // update user name if there is incoming message icon
    elmFullName = elm.getAttribute('data-fullname');

    if (elmFullName !== "All") {
        elm.innerText = elmFullName;
    }

    if (elm.getAttribute('data-new') !== '0') {
        elm.innerText += ' (' + elm.getAttribute('data-new') + ')';
    }
}

function userPressed(elm) {
    currentUserElm = elm;

    elm.setAttribute('data-new', '0');
    updateUserBtnName(elm);

    console.log('elm', elm);
    let fullName = elm.getAttribute('data-fullname');
    
    if(fullName === 'All')
    {
        chatTitleElm.innerText = 'All-Chat'
    } 
    else {
        fullName = elm.getAttribute('data-completename');
        chatTitleElm.innerText = fullName;
    }

    // clean current messages in pot
    let messagesElm = document.getElementById('messages');
    messagesElm.innerHTML = '';

    // retrieve messages for user if not done yet
    if (!elm.lastMessages) {
        let fromId = elm.getAttribute('data-id');
        loadMessages(elm, fromId === '0' ? null : fromId);
    }

    // otherwise the socket did already append the messages to display 
    else {
        for (let index = 0; index < elm.lastMessages.length; ++index) {
            const msg = elm.lastMessages[index];
            handleIncomingMessage(msg);
        }
    }
}

function sendPressed(elm) {
    let textarea = document.getElementById('message');
    if(textarea.value !== '\n') 
    { 
        let data = {
            text: textarea.value
        };

        if (currentUserElm.getAttribute('data-id') !== '0') {
            data.toId = parseInt(currentUserElm.getAttribute('data-id'));
        }
        
        
        io.emit('message', data);
    }
    textarea.value = '';
}

function delete_cookie() {
    document.cookie = '_wab_auth_jwt' + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}


io.on('message', (data) => {
    console.log('incoming message');
    handleIncomingMessage(data);
});

function handleIncomingMessage(data) {
    console.log('handling message');
    let elmId = null;

    // check incoming message has data.to === null or not inside, means main group
    if (!data.to) {
        if (currentUserElm.getAttribute('data-id') !== '0') {
            elmId = 'all';
        }
    }
    else if (currentUserId !== data.from.id && currentUserElm.getAttribute('data-id') != data.from.id) {
        elmId = data.from.id;
    }

    // check incoming message should set new message
    if (elmId !== null) {
        let elm = document.getElementById('btn-chat-' + elmId);
        if (elm) {
            let newMessageCount = parseInt(elm.getAttribute('data-new')) + 1;
            elm.setAttribute('data-new', newMessageCount);
            if (elm.lastMessages) {
                elm.lastMessages.push(data);
            }
            updateUserBtnName(elm);
        }
    }
    else {
        let msg = document.createElement('DIV');
        msg.classList.add('message-container');

        

        if(currentUserId === data.from.id)
        {
            msg.innerHTML = `<div class="message-container">
                            <div class="message-text self">
                                <div class="msgContent">${data.text}</div>
                                <div class="message-origin self">${data.from.displayName}</div>
                            </div>   
                        </div>`
        }
        else 
        {
            msg.innerHTML = `<div class="message-container">
                            <div class="message-text other">
                                <div class="msgContent">${data.text}</div>
                                <div class="message-origin other">${data.from.displayName}</div>
                            </div>   
                        </div>`
        }


        

        // messagesElm.appendChild(msgText);
        // messagesElm.appendChild(msgFullname);

        messagesElm.appendChild(msg);
        messagesElm.scrollTop = messagesElm.scrollHeight;
    }
}

messagesElm.addEventListener('scroll', function () {

    if (this.scrollTop === 0) {
        let fromId = currentUserElm.getAttribute('data-id');
        loadMessages(currentUserElm, fromId === '0' ? null : fromId);
    }
});

textarea.altActive = false;
textarea.addEventListener('keydown', function (event) {
    if (event.keyCode === 18) {
        textarea.altActive = true;
    }
});

textarea.addEventListener('keyup', function (event) {
    if (event.keyCode === 18) {
        textarea.altActive = false;
    }

    if (event.keyCode === 13 && textarea.altActive === false) {
        sendPressed();
    }
    else if (event.keyCode === 13 && textarea.altActive === true) {
        textarea.value += '\n';
    }
});

loadMessages(currentUserElm);




//////// Drag and Drop for KanBan
let sortableLists = sortable('.tasks', {
    forcePlaceholderSize: true,
    placeholderClass: 'task-placeholder',
    connectWith: 'tasks',
    //hoverClass: 'dragging',

});

for (let index = 0; index < sortableLists.length; index++) {
    const list = sortableLists[index];
    list.addEventListener('sortstart', function (e) {
        //placeholder
        //currently moving task gets disabled for other users
    });

    list.addEventListener('sortstop', function (e) {

    });

    list.addEventListener('sortupdate', function (e) {
        let item = e.detail.item;
        let target = e.target;

        //let startSort = e.detail.origin.itemsBeforeUpdate.indexOf(item) + 1; 
        let destinationSort = e.detail.destination.items.indexOf(item) + 1;

        let line = item.querySelector('.line');

        if (line) {
            line.style.background = target.getAttribute('data-workflow-color');
        }

        io.emit('task/move', {
            id: item.getAttribute('data-id'),
            workflowId: target.getAttribute('data-workflow-id'),
            endSort: destinationSort
        })
    });
}

io.on('task/move', (data) => {
    let item = document.querySelector('.task[data-id="' + data.id + '"]');
    if (item) {
        let taskList = document.querySelector('.tasks[data-workflow-id="' + data.workflowId + '"]');
        if (taskList) {
            let index = data.sort;
            if (taskList.children.length <= index) {
                taskList.appendChild(item);
            }
            else {
                let currentIndex = Array.prototype.indexOf.call(taskList.children, item);

                if (index !== 0 && currentIndex !== -1 && index >= currentIndex) {
                    index = index + 1;
                }


                let sibling = taskList.children[index];
                taskList.insertBefore(item, sibling);


                let line = item.querySelector('.line');

                if (line) {
                    line.style.background = taskList.getAttribute('data-workflow-color');
                }
            }
        }
    }
})

