function projectPressed(elm) {
    const projectId = elm.getAttribute('data-project-id');

    window.location = `/project/${projectId}`;
}