// 'use strict';
// module.exports = (sequelize, DataTypes) => {
//   const Message = sequelize.define('Message', {
//     text: DataTypes.STRING
//   }, {});
//   Message.associate = function(models) {
//     // associations can be defined here
//   };
//   return Message;
// };

'use strict';
module.exports = (sequelize, DataTypes) => {
    const Message = sequelize.define('Message', {
        text: {
            type: DataTypes.STRING(400),
            allowNull: false,
        }
    }, {
        tableName: 'message'
    });
    Message.associate = function (models) {
        Message.belongsTo(models.User, {
            as: 'from',
            foreignKey: 'fromId'
        });

        Message.belongsTo(models.User, {
            as: 'to',
            foreignKey: 'toId'
        });
    };
    return Message;
};