// 'use strict';
// module.exports = (sequelize, DataTypes) => {
//   const Task = sequelize.define('Task', {
//     name: DataTypes.STRING
//   }, {});
//   Task.associate = function(models) {
//     // associations can be defined here
//   };
//   return Task;
// };

'use strict';

module.exports = (sequelize, DataTypes) => {
    const Task = sequelize.define('Task', { 
        name: {
            type: DataTypes.STRING(120),
            allowNull: false
        }, 
        text: {
            type: DataTypes.STRING(400),
            allowNull: false
        },
        sort: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 999999
        },
        deadline: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: null
        }
    }, {
        tableName: 'task'
    });

    Task.associate = function (models) {
        Task.belongsTo(models.User,{
            as: 'creator',
            foreignKey: 'creatorId'
        });

        Task.belongsTo(models.User,{
            as: 'assignedTo',
            foreignKey: 'assignedToId'
        });

        Task.belongsTo(models.Project, {
            as: 'project',
            foreignKey: 'projectId'
        });

        Task.belongsTo(models.Workflow, {
            as: 'workflow',
            foreignKey: 'workflowId'
        });
    }
    return Task;
}