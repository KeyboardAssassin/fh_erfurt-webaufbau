'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.sequelize.transaction(async (transaction) => {
            await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS=0;', { transaction });
            await queryInterface.addColumn(
                'task',
                'workflowId',
                {
                    type: Sequelize.INTEGER,
                    references: {
                        model: {
                            tableName: 'workflow',
                        },
                        key: 'id'
                    },
                    allowNull: false
                },
                { transaction }
            );

            const [projects] = await queryInterface.sequelize.query('SELECT * FROM `project`;', { transaction });

            for(let index = 0; index < projects.length; index++)
            {
                const project = projects[index];
                await queryInterface.bulkInsert('workflow', [{
                    name: 'Backlog',
                    color: '#2F8E7E',
                    sort: 1,
                    projectId: project.id,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },{
                    name: 'In Progress',
                    color: '#2F8E7E',
                    sort: 2,
                    projectId: project.id,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },{
                    name: 'Review',
                    color: '#2F8E7E',
                    sort: 3,
                    projectId: project.id,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },{
                    name: 'Finished',
                    color: '#2F8E7E',
                    sort: 4,
                    projectId: project.id,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                }], {transaction});

                const [workflows] = await queryInterface.sequelize.query('SELECT * FROM `workflow` WHERE `projectId` = '+project.id+';', { transaction });
                const workflow = workflows[0];

                const [updateResult] = await queryInterface.sequelize.query('UPDATE `task` SET `workflowId` = '+workflow.id+' WHERE `projectId` = '+project.id+';', { transaction });
            }

            return queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS=1;', { transaction });
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.sequelize.transaction(async (transaction) => {
            await queryInterface.removeColumn('task', 'workflowId');
        })
    }
}