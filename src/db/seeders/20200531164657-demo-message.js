'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('message', [{
      text: 'Hallo Jonas!',
      fromId: 1,
      toId: 2,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, 
    {
      text: 'Whatup Daniel?',
      fromId: 2,
      toId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      text: 'Hallo Gruppenchat!',
      fromId: 2,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      text: 'Willkommen zurück im Chat Jonas!',
      fromId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('message', null, {});
  }
};
