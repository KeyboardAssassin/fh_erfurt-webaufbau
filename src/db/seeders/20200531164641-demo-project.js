'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('project', [{
            id: 1,
            name: 'Webaufbau',
            createdAt: new Date(),
            updatedAt: new Date(),
        }, {
            id: 2,
            name: 'Medienrecht',
            createdAt: new Date(),
            updatedAt: new Date(),
        }, {
            id: 3,
            name: 'Graphentheorie',
            createdAt: new Date(),
            updatedAt: new Date(),
        }, {
            id: 4,
            name: 'Softwaretechnik 2',
            createdAt: new Date(),
            updatedAt: new Date(),
        }
        ], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('project', null, {});
    }
};