'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

      return queryInterface.bulkInsert('user', [{
        id: 1,
        firstName: 'Daniel',
        lastName: 'Cerhak',
        email: 'daniel.cerhak@fh-erfurt.de',
        createdAt: new Date(),
        updatedAt: new Date(),
        passwordHash: '$2a$10$9kRUIhecJdEaeS5a/qh1JOvXJVCt9EcAKt8G/5vfCoNBU6.2T3Ini', // bycrypt 10 12345678
        permission: 0b1111111111111111,
        token: null
      },
      {
        id: 2,
        firstName: 'Jonas',
        lastName: 'Weigelt',
        email: 'jonas.weigelt@fh-erfurt.de',
        createdAt: new Date(),
        updatedAt: new Date(),
        passwordHash: '$2a$10$9kRUIhecJdEaeS5a/qh1JOvXJVCt9EcAKt8G/5vfCoNBU6.2T3Ini', // bycrypt 10 12345678
        permission: 0b1111111111111111,
        token: null
      },{
        id: 3,
        firstName: 'Kristof',
        lastName: 'Friess',
        email: 'kristof.friess@fh-erfurt.de',
        createdAt: new Date(),
        updatedAt: new Date(),
        passwordHash: '$2a$10$9kRUIhecJdEaeS5a/qh1JOvXJVCt9EcAKt8G/5vfCoNBU6.2T3Ini', // bycrypt 10 12345678
        permission: 0b1111111111111111,
        token: null
      },{
        id: 4,
        firstName: 'Rolf',
        lastName: 'Kruse',
        email: 'rolf.kruse@fh-erfurt.de',
        createdAt: new Date(),
        updatedAt: new Date(),
        passwordHash: '$2a$10$9kRUIhecJdEaeS5a/qh1JOvXJVCt9EcAKt8G/5vfCoNBU6.2T3Ini', // bycrypt 10 12345678
        permission: 0b1111111111111111,
        token: null
      },], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('user', null, {});
  }
};