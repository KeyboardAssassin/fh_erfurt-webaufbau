'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('task', [{
      name: 'Webdesign fertigstellen',
      text: 'Loginscreen designen + Projektübersicht fertigstellen!',
      creatorId: 1,
      assignedToId: 1,
      projectId: 1,
      workflowId: 1,
      sort: 1,
      deadline: '2020-07-29 12:00:00',
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      name: 'Projektabgabe',
      text: 'Das Projekt im Moodle hochladen!',
      creatorId: 1,
      assignedToId: 1,
      projectId: 1,
      workflowId: 1,
      sort: 2,
      deadline: '2020-07-29 12:00:00',
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      name: 'Evaluierung des Moduls',
      text: 'Im Moodle das Projekt evaluieren!',
      creatorId: 1,
      assignedToId: 1,
      projectId: 1,
      workflowId: 1,
      sort: 3,
      deadline: '2020-07-29 12:00:00',
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('task', null, {});
  }
};
