'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('workflow', [{
            name: 'To do',
            color: '#2F8E7E',
            sort: 1,
            projectId: 1,
            createdAt: new Date(),
            updatedAt: new Date(),
        },{
            name: 'In Bearbeitung',
            color: '#2F8E7E',
            sort: 2,
            projectId: 1,
            createdAt: new Date(),
            updatedAt: new Date(),
        },{
            name: 'Besprechen',
            color: '#2F8E7E',
            sort: 3,
            projectId: 1,
            createdAt: new Date(),
            updatedAt: new Date(),
        },{
            name: 'Fertig',
            color: '#2F8E7E',
            sort: 4,
            projectId: 1,
            createdAt: new Date(),
            updatedAt: new Date(),
        },{
            name: 'To do',
            color: '#2F8E7E',
            sort: 1,
            projectId: 2,
            createdAt: new Date(),
            updatedAt: new Date(),
        },{
            name: 'In Bearbeitung',
            color: '#2F8E7E',
            sort: 2,
            projectId: 2,
            createdAt: new Date(),
            updatedAt: new Date(),
        },{
            name: 'Besprechen',
            color: '#2F8E7E',
            sort: 3,
            projectId: 2,
            createdAt: new Date(),
            updatedAt: new Date(),
        },{
            name: 'Fertig',
            color: '#2F8E7E',
            sort: 4,
            projectId: 2,
            createdAt: new Date(),
            updatedAt: new Date(),
        },{
            name: 'To do',
            color: '#2F8E7E',
            sort: 1,
            projectId: 3,
            createdAt: new Date(),
            updatedAt: new Date(),
        },{
            name: 'In Bearbeitung',
            color: '#2F8E7E',
            sort: 2,
            projectId: 3,
            createdAt: new Date(),
            updatedAt: new Date(),
        },{
            name: 'Besprechen',
            color: '#2F8E7E',
            sort: 3,
            projectId: 3,
            createdAt: new Date(),
            updatedAt: new Date(),
        },{
            name: 'Fertig',
            color: '#2F8E7E',
            sort: 4,
            projectId: 3,
            createdAt: new Date(),
            updatedAt: new Date(),
        }], {});
    }
}