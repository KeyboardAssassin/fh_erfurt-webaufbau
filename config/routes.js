/**
 * @author Daniel Cerhak & Jonas Weigelt <daniel.cerhak@fh-erfurt.de & jonas.weigelt@fh-erfurt.de>
 * @version 1.0.0
 * @description Includes all routes defined by path and controller class
 */

const PagesController = require('../controllers/pagesController.js');
const ApiUsersController = require('../controllers/api/usersController.js');
const ApiTasksController = require('../controllers/api/tasksController.js');
const ApiProjectsController = require('../controllers/api/projectsController.js');
const ApiMessagesController = require('../controllers/api/messagesController.js');

let routes = {
    'pages': {
        controller: PagesController,
        actions: [
            { path: '/', action: 'projects', method: 'get' },
            { path: '/project/:id', action: 'index', method: 'get' },
            { path: '/imprint', action: 'imprint', method: 'get' },
            { path: '/privacy', action: 'privacy', method: 'get' },
            { path: '/sign-in', action: 'signin', method: 'get' }
        ]
    },
    'api/users': {
        controller: ApiUsersController,
        actions: [
            { path: '/api/users', action: 'index', method: 'get' },
            { path: '/api/signin', action: 'signin', method: 'post' },
            { path: '/api/signup', action: 'signup', method: 'post' },
            { path: '/api/users/:id', action: 'show', method: 'get' },
            { path: '/api/users/:id', action: 'update', method: 'put' },
            { path: '/api/users/:id', action: 'delete', method: 'delete' }
        ]
    },
    'api/tasks': {
        controller: ApiTasksController,
        actions: [
            { path: '/api/tasks', action: 'index', method: 'get' },
            { path: '/api/tasks', action: 'create', method: 'post' },
            { path: '/api/tasks/:id', action: 'show', method: 'get' },
            { path: '/api/tasks/:id', action: 'update', method: 'put' },
            { path: '/api/tasks/:id', action: 'delete', method: 'delete' }
        ]
    },
    'api/projects': {
        controller: ApiProjectsController,
        actions: [
            { path: '/api/projects', action: 'index', method: 'get' },
            { path: '/api/projects', action: 'create', method: 'post' },
            { path: '/api/projects/:id', action: 'show', method: 'get' },
            { path: '/api/projects/:id', action: 'delete', method: 'delete' }
        ]
    },
    'api/messages': {
        controller: ApiMessagesController,
        actions: [
            { path: '/api/messages', action: 'index', method: 'get' },
            { path: '/api/messages', action: 'create', method: 'post' },
            { path: '/api/messages/:id', action: 'update', method: 'put' },
            { path: '/api/messages/:id', action: 'show', method: 'get' },
            { path: '/api/messages/:id', action: 'delete', method: 'delete' }
        ]
    },
}

module.exports = routes;