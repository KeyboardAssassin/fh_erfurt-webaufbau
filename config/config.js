module.exports = {
    secret: process.env.SECRET || 'Y3MHznKERKRDh3CEGF8uEcXKnf28QPu6g97Ju563dyreDnzQk2Vs2hCtaaZUWFVy',
    jwtExpiresAfter: 60 * 60 * 1000, // 1 hour
    cookieName: '_wab_auth_jwt',
    db: {
        username: process.env.DB_USER || 'root',
        password: process.env.DB_PASSWORD || '',
        database: process.env.DB_NAME || 'taskboard',
        host: process.env.DB_HOST || 'localhost',
        dialect: 'mysql',
    },
    username: process.env.DB_USER || 'root',
    password: process.env.DB_PASSWORD || '',
    database: process.env.DB_NAME || 'taskboard',
    host: process.env.DB_HOST || 'localhost',
    dialect: 'mysql',
    permissions: {
        user: 0,
        moderator: 8,
        admin: 16
    },
    minify: true,
};