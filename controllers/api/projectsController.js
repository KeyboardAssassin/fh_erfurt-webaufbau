/**
 * @author Daniel Cerhak <daniel.cerhak@fh-erfurt.de>
 * @version 1.0.0
 * @description ProjectsController
 */

const Controller = require('../mainController.js');
const ApiError = require('../../core/error.js');


class ApiProjectsController extends Controller {
    constructor(...args) {
        super(...args);

        const self = this;

        self.format = Controller.HTTP_FORMAT_JSON;

        self.before(['*'], function (next) {
            if (self.req.authorized === true) {
                next();
            }
            else {
                self.render({}, {
                    statusCode: 401
                });
            }
        });
    }

    async actionIndex() {
        const self = this;

        let projects = [];
        let total = 0;
        let error = null;

        let paging = self.paging();

        try {
            const {count, rows } = await self.db.Project.findAndCountAll({
                order: [
                    ['id', 'ASC'],
                ],
                limit: paging.limit,
                offset: paging.offset,
                attributes: ['id', 'name', 'createdAt', 'updatedAt'],
                include: self.db.Project.extendInclude
            });

            total = count;
            projects = rows;
        }
        catch (err) {
            error = err;
            console.log(err);
        }

        if (error) {
            self.render({
                details: error
            }, {
                statusCode: 500
            });
        }
        else {
            self.render({
                projects: projects,
                _meta: self.meta(paging, total)
            }, {
                statusCode: 200
            });
        }
    }

    async actionShow() {
        const self = this;

        let id = self.param('id');
        let project = null;
        let error = null;

        try {
            project = await self.db.Project.findOne({
                where: {
                    id: id
                },
                attributes: ['id', 'name', 'createdAt', 'updatedAt'],
                include: self.db.Project.extendInclude
            });

            if(!project)
            {
                throw new ApiError(`No project found for id ${id}`);
            }
        }
        catch (err) {
            error = err;
        }

        if (error) {
            self.handleError(error);
            self.render({
                statusCode: 500
            });
        }
        else {
            self.render({
                project: project,
                statusCode: 200
            });
        }
    }

    async actionCreate() {
        const self = this;

        let remoteData = self.param('project');

        let project = null;
        let error = null;

        try {
            project = await self.db.sequelize.transaction(async (t) => {
                let newProject = self.db.Project.build();
                newProject.writeRemotes(remoteData);

                await newProject.save({
                    transaction: t
                });

                return newProject;                                                 // Gibt den Fehler "newTask is not defined" aus
            });

            for(let index = 1; index < 5; index++)
            {
                await self.db.sequelize.transaction(async(t) => {
                    let newWorkflow = self.db.Workflow.build();
                    switch(index){
                        case 1:
                            newWorkflow.name = 'Backlog';
                            break;
                        case 2:
                            newWorkflow.name = 'In Progress';
                            break;
                        case 3:
                            newWorkflow.name = 'Review';
                            break;
                        case 4:
                            newWorkflow.name = 'Finished';
                            break;
                    }

                    newWorkflow.color = '#2F8E7E';
                    newWorkflow.sort = index;
                    newWorkflow.projectId = project.id;

                    await newWorkflow.save({
                        transaction: t
                    });
                })
            }

            console.log("Successfully created new project");
        }
        catch (err) {
            error = err;
        }

        if (error !== null) {
            console.error(error);
            self.render({
                details: error
            }, {
                statusCode: 500
            });
        }
        else {
            self.render({
                project: project,
                statusCode: 201
            });
        }
    }

    async actionDelete() {
        const self = this;

        let id = self.param('id');
        let project = null;
        let error = null;
        
        try {
            
            project = await self.db.Project.findOne({
                where: {
                    id: id
                }
            });
            
            
            if(!project)
            {
                throw new ApiError(`Cant find project with id ${id}`, 404);
            }
            else
            {

                project.name = 'deleted project';

                await project.save();

            }
        }
        catch (err) {
            error = err;
        }

        if (error) {
            self.handleError(error);
            self.render({
                statusCode: 500
            });
        }
        else {
            self.render({
                project: project
            }, {
                statusCode: 200
            });
        }
    }

}

module.exports = ApiProjectsController;