/**
 * @author Daniel Cerhak <daniel.cerhak@fh-erfurt.de>
 * @version 1.0.0
 * @description MessagesController
 */

const Controller = require('../mainController.js');
const { Op } = require('sequelize');
const ApiError = require('../../core/error.js');

class ApiMessagesController extends Controller {
    constructor(...args) {
        super(...args);

        const self = this;

        self.format = Controller.HTTP_FORMAT_JSON;

        self.before(['*'], function (next) {
            if (self.req.authorized === true) {
                next();
            }
            else {
                self.render({}, {
                    statusCode: 401
                });
            }
        });
    }

    async actionIndex() {
        const self = this;

        let messages = [];
        let error = null;

        //retrieving params
        let fromId = self.param('fromId') || null;
        let total = 0;
        let paging = self.paging();



        try {
            let where = {};

            //search only messages from a defined person
            if(fromId !== null)
            {
                where = {
                    [Op.or] : [
                        {
                            fromId : fromId,
                            toId: self.req.user.id
                        },
                        {
                            toId: fromId,
                            fromId: self.req.user.id
                        },
                    ]
                };
            }
            else
            {
                where = {
                    toId: {
                        [Op.is]: null,
                    }
                };
            }

            const {count, rows } = await self.db.Message.findAndCountAll({
                where: where,
                order: [
                    ['id', 'DESC'],
                ],
                limit: paging.limit,
                offset: paging.offset,
                attributes: ['id', 'text', 'createdAt', 'updatedAt'],
                include: self.db.Message.extendInclude
            });

            total = count;
            messages = rows;
        }
        catch (err) {
            error = err;
            console.log(err);
        }

        if (error) {
            self.render({
                details: error
            }, {
                statusCode: 500
            });
        }
        else {
            self.render({
                messages: messages,
                _meta: self.meta(paging, total)
            }, {
                statusCode: 200
            });
        }
    }

    async actionShow() {
        const self = this;

        let error = null;

        //retrieving params
        let id = self.param('id');
        let message = null;

        try {
            message = await self.db.Message.findOne({
                where: {
                    fromId: self.req.user.id,
                    id: id
                },
                attributes: ['id', 'text', 'createdAt', 'updatedAt'],
                include: self.db.Message.extendInclude
            });

            if(!message)
            {
                throw new ApiError(`No message found for id ${id}`, 404);
            }
        }
        catch (err) {
            error = err;
            console.log(err);
        }

        if (error) 
        {
            self.handleError(error);
            self.render({
                statusCode: 500
            });
        }
        else 
        {
            self.render({
                message: message,
                statusCode: 200
            });
        }
    }

    async actionUpdate()
    {
        const self = this;

        let error = null;

        //retrieving params
        let id = self.param('id');
        let message = null;

        try
        {
            let remoteMessage = self.param('message');
            if(!remoteMessage)
            {
                throw new ApiError('Message object is missing, please check your body structure', 400);
            }

            if(!remoteMessage.text)
            {
                throw new ApiError('Message object has no text, please check your body structure', 400);
            }

            message = await self.db.Message.findOne({
                where: {
                    fromId: self.req.user.id,
                    id: id
                },
                attributes: ['id', 'text', 'createdAt', 'updatedAt'],
                include: self.db.Message.extendInclude
            });

            if(!message)
            {
                throw new ApiError('No message found for delete', 404);
            }
            else 
            {
                message.text = remoteMessage.text;
                await message.save();
            }
        }
        catch(err)
        {
            error = err;
            console.log(err);
        }

        if(error)
        {
            self.handleError(error);
            self.render({
                statusCode: 500
            });
        }
        else
        {
            self.render({
                message: message,
                statusCode: 201
            });
        }
    }


    async actionCreate() {
        const self = this;

        let remoteData = self.param('message');

        let message = null;
        let error = null;

        try {
            message = await self.db.sequelize.transaction(async (t) => {
                let newMessage = self.db.Message.build();
                newMessage.writeRemotes(remoteData);

                await newMessage.save({
                    transaction: t
                });
                return newMessage;                                                 // Gibt den Fehler "newTask is not defined" aus
            });
            console.log("Erfolgreich in die DB eingefügt!");
        }
        catch (err) {
            error = err;
        }

        if (error !== null) {
            console.error(error);
            self.render({
                details: error
            }, {
                statusCode: 500
            });
        }
        else {
            self.render({
                message: message,
                statusCode: 201
            });
        }
    }

    async actionDelete() {
        const self = this;

        let error = null;

        // retrieving params
        let id = self.param('id');

        try
        {
            let message = await self.db.Message.findOne({
                where: {
                    fromId: self.req.user.id,
                    id: id
                }
            });

            if(!message)
            {
                throw new ApiError('No message found for delete', 404);
            }
            else
            {
                message.text = 'deleted';
                await message.save();
            }
        }
        catch(err)
        {
            error = err;
            console.log(err);
        }

        if(error)
        {
            self.handleError(error);
            self.render({
                statusCode: 500
            });
        }
        else
        {
            self.render({
                statusCode: 204
            });
        }
    }

}

module.exports = ApiMessagesController;
