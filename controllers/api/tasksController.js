/**
 * @author Daniel Cerhak <daniel.cerhak@fh-erfurt.de>
 * @version 1.0.0
 * @description API tasks controller used to handle the model resource
 */

const Controller = require('../mainController.js');
const ApiError = require('../../core/error.js');


class ApiTasksController extends Controller {

    constructor(...args) {
        super(...args);

        const self = this;

        self.format = Controller.HTTP_FORMAT_JSON;

        self.before(['*'], function (next) {
            if (self.req.authorized === true) {
                next();
            }
            else {
                self.render({}, {
                    statusCode: 401
                });
            }
        });

    }

    async actionIndex() {
        const self = this;

        let tasks = [];
        let error = null;

        let paging = self.paging();
        let total = 0;

        try {
            const {count, rows } = await self.db.Task.findAndCountAll({
                order: [
                    ['id', 'ASC'],
                ],
                limit: paging.limit,
                offset: paging.offset,
                attributes: ['id', 'name', 'text','deadline', 'sort', 'createdAt', 'updatedAt'],
                include: self.db.Task.extendInclude
            });

            total = count;
            tasks = rows;
        }
        catch (err) {
            error = err;
            console.log(err);
        }

        if (error !== null) {
            console.error(error);
            self.render({
                details: error
            }, {
                statusCode: 500
            });
        }
        else {
            self.render({
                tasks: tasks,
                _meta: self.meta(paging, total)
            }, {
                statusCode: 200
            });
        }
    }

    async actionShow() {
        const self = this;

        let taskId = self.param('id');
        let task = null;
        let error = null;

        try {
            task = await self.db.Task.findOne({
                where: {
                    id: taskId
                },
                attributes: ['id', 'name', 'text', 'deadline', 'sort', 'createdAt', 'updatedAt'],
                include: self.db.Task.extendInclude
            });
        }
        catch (err) {
            error = err;
        }

        if (error) {
            self.render({
                details: error
            }, {
                statusCode: 500,
                statusCode: 200
            });
        }
        else {
            self.render({
                task: task
            });
        }
    }

    async actionCreate() {
        const self = this;

        let remoteData = self.param('task');

        let task = null;
        let error = null;

        try {
            task = await self.db.sequelize.transaction(async (t) => {
                let newTask = self.db.Task.build();
                newTask.writeRemotes(remoteData);

                await newTask.save({
                    transaction: t
                });
                return newTask;                                                 // Gibt den Fehler "newTask is not defined" aus
            });
            console.log("Successfully created new task");
        }
        catch (err) {
            error = err;
        }

        if (error !== null) {
            console.error(error);
            self.render({
                details: error
            }, {
                statusCode: 500
            });
        }
        else {
            self.render({
                task: task,
                statusCode: 201
            });
        }
    }

    async actionUpdate() {
        const self = this;

        let error = null;

        //retrieving params
        let id = self.param('id');
        let task = null;

        try
        {
            let remoteTask = self.param('task');
            if(!remoteTask)
            {
                throw new ApiError('Task object is missing, please check your body structure', 400);
            }

            if(!remoteTask.name && !remoteTask.text && !remoteTask.assignedTo && !remoteTask.sort)
            {
                throw new ApiError('Task object has no content, please check your body structure', 400);
            }

            task = await self.db.Task.findOne({
                where: {
                    id: id
                },
                attributes: ['id', 'name', 'text', 'deadline', 'sort', 'assignedToId', 'creatorId', 'createdAt', 'updatedAt'],
                include: self.db.Task.extendInclude
            });


            if(!task)
            {
                throw new ApiError('No task found for update', 404);
            }
            else 
            {
                task.writeRemotes(remoteTask);
                await task.save();
            }
        }
        catch(err)
        {
            error = err;
        }

        if(error)
        {
            console.log(error);
            self.handleError(error);
        }
        else
        {
            self.render({
                task: task,
                statusCode: 201
            });
        }
    }

    async actionDelete() {
        const self = this;

        let taskId = self.param('id');
        let task = null;
        let error = null;

        try {
            task = await self.db.Task.destroy({
                where: {
                    id: taskId
                }
            });
        }
        catch (err) {
            error = err;
        }

        if (error) {
            self.render({
                details: error
            }, {
                statusCode: 500
            });
        }
        else {

            self.render({
                task: task
            }, {
                statusCode: 204
            });
        }
    }
}

module.exports = ApiTasksController;