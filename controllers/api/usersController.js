/**
 * @author Daniel Cerhak <daniel.cerhak@fh-erfurt.de>
 * @version 1.0.0
 * @description API users controller used to handle the model resource
 */

const Controller = require('../mainController.js');
const Passport = require('../../core/passport.js');
const ApiError = require('../../core/error.js');
const config = require('../../config/config.js');

class ApiUsersController extends Controller {

    constructor(...args) {
        super(...args);

        const self = this;

        self.format = Controller.HTTP_FORMAT_JSON;

        self.before(['*', '-signin', '-signup'], function (next) {
            if (self.req.authorized === true) {
                next();
            }
            else {
                self.render({}, {
                    statusCode: 401
                });
            }
        });

    }

    async actionIndex() {
        const self = this;

        let users = [];
        let error = null;

        let paging = self.paging();
        let total = 0;

        try {
            const {count, rows } = await self.db.User.findAndCountAll({
                order: [
                    ['id', 'ASC'],
                ],
                limit: paging.limit,
                offset: paging.offset,
                attributes: ['id', 'firstName', 'lastName', 'email', 'permission', 'createdAt', 'updatedAt'],
                include: self.db.User.extendInclude
            });

            total = count;
            users = rows;
        }
        catch (err) {
            error = err;
        }


        if (error !== null) {
            self.handleError(error);
        }
        else {
            self.render({
                users: users,
                _meta: self.meta(paging, total)
            }, {
                statusCode: 200
            });
        }


    }

    async actionShow() {
        const self = this;

        let id = self.param('id');
        let user = null;
        let error = null;

        try {
            user = await self.db.User.findOne({
                where: {
                    id: id
                }
            });

            if(!user)
            {
                throw new ApiError(`No user found for id ${id}`, 404)
            }
        }
        catch (err) {
            error = err;
        }

        if (error) {
            self.handleError(error);
        }
        else {
            self.render({
                user: user
            }, {
                statusCode: 200
            });
        }
    }

    async actionSignin() {
        const self = this;

        let remoteData = self.param('user') || {};

        let user = null;
        let error = null;

        try {
            user = await self.db.User.findOne({
                where: {
                    email: remoteData.email
                }
            });

            if (!user || !Passport.comparePassword(remoteData.password, user.passwordHash)) {
                throw new ApiError('No user found for this email of password.', 404);
            }
        }
        catch (err) {
            error = err;
        }

        if (error) {
            console.log('Error', error);
            self.handleError(error);
        }
        else {
            let token = Passport.authorizeUserWithCookie(self.req, self.res, user.id);
            user.token = token;
            await user.save();
            self.render({
                token: token
            }, {
                statusCode: 201
            });
        }
    }

    async actionSignup() {
        const self = this;

        let remoteData = self.param('user');

        let user = null;
        let error = null;

        try {
            user = await self.db.sequelize.transaction(async (t) => {

                let sameMail = await self.db.User.findOne({
                    where: {
                        email: remoteData.email
                    },
                    lock: true,
                    transaction: t
                });

                if (sameMail) {
                    throw new ApiError('Mail already in use', 409);
                }

                let newUser = self.db.User.build();
                newUser.writeRemotes(remoteData);
                await newUser.save({
                    transaction: t
                });

                return newUser;
            });
        }
        catch (err) {
            error = err;
        }

        if (error) {
            self.handleError(error)
        }
        else {
            self.render({
                user: user
            }, {
                statusCode: 201
            });
        }
    }

    async actionUpdate() {
        const self = this;

        let error = null;

        //retrieving params
        
        let user = null;
        let id = self.param('id');

        try
        {
            
            if(self.req.user.permission <= config.permissions.moderator && self.req.user.id !== id)
            {
                throw new ApiError('Not authorized to update other accounts than your own', 401);
            }
            
    

            let remoteUser = self.param('user');
            if(!remoteUser)
            {
                throw new ApiError('User object is missing, please check your body structure', 400);
            }

            if(!remoteUser.firstName && !remoteUser.lastName && !remoteUser.email && !remoteUser.password && !remoteUser.firstName && !remoteUser.permission)
            {
                throw new ApiError('User object contains no information for updating the account, please check your body structure', 400);
            }

            
            user = await self.db.User.findOne({
                where: {
                    id: id,
                },
                attributes: ['id', 'firstName', 'lastName', 'email', 'passwordHash', 'createdAt', 'updatedAt'],
                include: self.db.User.extendInclude
            });
        
            

            if(!user)
            {
                throw new ApiError(`No user with id ${id} found for update`, 404);
            }
            else 
            {
                for(let property in remoteUser)
                {
                    if(property === 'password')
                    {
                        user.passwordHash = Passport.hashPassword(remoteUser.password);
                    }
                    else if (property === 'email')
                    {
                        let sameMail = await self.db.User.findOne({
                            where: {
                                email: remoteUser.email
                            },
                            lock: true
                        });

                        if (sameMail && sameMail !== user.email)
                        {
                            throw new ApiError(`Mail \"${remoteUser.email}\" is already in use`, 409)
                        }
                        else
                        {
                            user.email = remoteUser.email;
                        }
                    }
                    else if(property === 'permission')
                    {
                        if(self.req.user.permission >= config.permissions.admin)
                        {
                            user.permission = remoteUser.permission;
                        }
                        else
                        {
                            throw new ApiError('Unauthorized to change permissions', 401);
                        }
                    }
                    else
                    {
                        user[element] = remoteMessage[element];
                    }
                };
                await user.save();
            }
        }
        catch(err)
        {
            error = err;
            console.log(err);
        }

        if(error)
        {
            self.handleError(error);
        }
        else
        {
            self.render({
                user: user,
                statusCode: 201
            });
        }
    }


    async actionDelete() {
        const self = this;

        let userId = self.param('id');
        let user = null;
        let error = null;
        
        try {
            
            user = await self.db.User.findOne({
                where: {
                    id: userId
                }
            });
            
            
            if(!user)
            {
                throw new ApiError(`Cant find user with id ${id}`, 404);
            }
            else
            {
                if(Number(userId) !== self.req.user.id)
                {
                    if(self.req.user.permission < config.permission.moderator || self.req.user.permission <= user.permission)
                    {
                        throw new ApiError(`Not authorized to delete account with id ${userId}`, 401);
                    }
                }

                user.firstName = 'deleted';
                user.lastName = 'user';
                user.email = 'deleted';
                user.permission = 0;
                user.passwordHash = 'aaa';

                await user.save();

                // delete user cookie if deleted own acc
            }
        }
        catch (err) {
            error = err;
        }

        if (error) {
            self.handleError(error);
        }
        else {
            self.render({
                user: user
            }, {
                statusCode: 204
            });
        }
    }
}


module.exports = ApiUsersController;