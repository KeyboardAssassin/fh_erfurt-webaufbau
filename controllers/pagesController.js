/**
 * @author Daniel Cerhak <daniel.cerhak@fh-erfurt.de>
 * @version 1.0.0
 * @description PagesController for default pages like home, login, privacy, terms ...
 */

const Controller = require('./mainController.js');

class PagesController extends Controller
{

    constructor(...args)
    {
        super(...args);

        const self = this;

        self.css('layout');

        self.before(['*', '-imprint', '-signin'], (next) => {
            
            if(self.req.authorized === true)
            {
                next();
            }
            else
            {
                self.redirect(self.urlFor('pages', 'signin'));
            }
        });

        self.before(['signin'], (next) => {
            
            if(self.req.authorized === true)
            {
                self.redirect(self.urlFor('pages', 'index'));
            }
            else
            {
                next();
            }
        });

        // self.before('index', (next) => {
        //     console.log('only index');

        //     next();
        // });
    }

    async actionProjects()
    {
        const self = this;
        self.js('projects');
        self.css('projects');

        const projects = await self.db.Project.findAll();

        self.render({
            title: 'Welcome!',
            projects: projects
        });
    }

    async actionIndex()
    {
        const self = this;
        self.js('html5sortable');
        self.js('_slideInPopover');
        self.js('index');
        self.css('index');

        const projectId = self.param('id');

        if(projectId)
        {
            const project = await self.db.Project.findOne({
                where: {
                    id: projectId
                }
            })

            const users = await self.db.User.findAll();
                
            const workflows = await self.db.Workflow.findAll({
                where: {
                    projectId: projectId
                },
                order: [
                    ['sort', 'ASC']
                ]
            });
    
            const workflowTasks = {};
    
            for (let index = 0; index < workflows.length; index++)
            {
                const workflow = workflows[index];
                workflowTasks[workflow.id] = await self.db.Task.findAll({
                    where: {
                        workflowId: workflow.id,
                    },
                    include: ['assignedTo'],
                    order: ['sort']
                });
            }
            
            self.render({
                title: project.name,
                project: project,
                users: users,
                workflows: workflows,
                workflowTasks: workflowTasks
            });
        }
        else
        {
            console.log('No project selected!');
        }
    }

    actionImprint()
    {
        const self = this;

        self.css('imprint');

        self.render({
            title: 'Imprint'
        });
    }
    actionPrivacy()
    {
        const self = this;
        
        self.css('privacy');

        self.render({
            title: 'Privacy'
        });
    }

    actionSignin()
    {
        const self = this;

        self.css('signin');
        self.js('signin');

        self.render({
            title: 'Login',
            navigation: false
        });
    }
}

module.exports = PagesController;