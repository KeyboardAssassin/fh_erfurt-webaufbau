/**
 * @author Daniel Cerhak & Jonas Weigelt <daniel.cerhak@fh-erfurt.de & jonas.weigelt@fh-erfurt.de>
 * @version 1.0.0
 * @description index.js is our launch start point to initialize the application with all needs.
 */

const SocketHandler = require('./core/socket.js');

const express  = require('express');
const app      = express();
const http     = require('http').createServer(app);
const io       = require('socket.io')();


// file handling
const Multer  =   require('multer');
const storage = Multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + file.originalname);
    }
})
const upload = Multer({storage: storage});


io.attach(http);

// write global configuration
global.cfg = require('./config/config.js');

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/api/files', upload.single('imageFile'));
app.use('/uploads', express.static('uploads'));

app.use('/assets', express.static('assets'));

const database = require('./core/database.js')();
const socket = new SocketHandler(io, database);
app.ioHandler = socket;

const routes = require('./config/routes.js');
const Router = require('./core/router.js');
const router = new Router(app, routes, database);
router.setup();




http.listen(process.env.PORT || 3000, function(){
    console.log('App listening at http://localhost:' + (process.env.PORT || 3000));
});