/**
 * @description Error for Custom API Error
 */

 class ApiError extends Error
 {
     constructor(message, statusCode)
     {
         super(message);
         this.statusCode = statusCode;
     }
 }

 module.exports = ApiError;