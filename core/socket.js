/**
 * @author Daniel Cerhak <daniel.cerhak@fh-erfurt.de>
 * @version 1.0.0
 * @description Core Routes handling, used to generate express routes and also helper methods for params and url creation
 */


const Passport = require('./passport.js');
const { Op } = require("sequelize");

class SocketHandler {
    constructor(io, db) {
        const self = this;

        self.db = db;
        self.io = io;
        self.sockets = {};

        io.use((socket, next) => {

            let handshakeData = socket.handshake;
            let tokenPayload = Passport.isAuthorized(handshakeData);

            if (tokenPayload === false || tokenPayload.exp < Date.now()) {
                next(new Error('unauthorized'));
            }
            else {
                self.db.User.findOne({
                    where: {
                        id: tokenPayload.id
                    }
                }).then((user) => {

                    if (!user) {
                        next(new Error('unauthorized'));
                    }
                    else {
                        // console.log('handshake', tokenPayload);
                        socket.user = user;
                        next();
                    }
                }).catch((err) => {
                    console.error(err);
                    next(new Error('unauthorized'));
                });
            }
        });

        self.initEvents();
    }

    findUserSocketById(id) {
        let socket = null;
        for (let key in this.sockets) {
            socket = this.sockets[key];
            if (socket.user && socket.user.id === id) {
                return socket;
            }
        }

        return null;
    }

    initEvents() {
        const self = this;

        self.io.on('connection', (socket) => {
            console.log('new client connected', socket.id);
            self.sockets[socket.id] = socket;

            socket.on('disconnect', () => {
                console.log('disconnect client', socket.id);
                if (self.sockets[socket.id]) {
                    delete self.sockets[socket.id];
                }
            });

            socket.on('message', async (data) => {

                //write incoming message to database
                let message = self.db.Message.build();
                message.writeRemotes(data);

                try {
                    message.fromId = socket.user.id;
                    await message.save();

                    let response = {
                        text: data.text,
                        from: {
                            displayName: socket.user.fullname(),
                            id: socket.user.id
                        },
                        time: message.createdAt
                    };

                    if (message.toId) {
                        response.to = {
                            id: message.toId
                        };

                        let userSocket = self.findUserSocketById(message.toId);
                        if (userSocket) {
                            response.to.displayName = userSocket.user.fullname();
                            userSocket.emit('message', response);
                        }
                        socket.emit('message', response);
                    }
                    else {
                        self.io.emit('message', response);
                    }
                }
                catch (err) {
                    console.log(err);
                    socket.emit('error', {
                        details: 'Could not save message, something went wrong',
                        message: data
                    })
                }

            });

            socket.on('task/move', async (data) => {

                let task = await self.db.Task.findOne({
                    where: {
                        id: data.id
                    }
                });



                if (task) {
                    console.log(task.sort);
                    if (task.workflowId !== data.workflowId) {
                        await self.db.Task.update({
                            sort: self.db.sequelize.literal('sort - 1')
                        }, {
                            where: {
                                sort: {
                                    [Op.gt]: task.sort
                                },
                                workflowId: task.workflowId
                            }
                        });

                        await self.db.Task.update({
                            sort: self.db.sequelize.literal('sort + 1')
                        }, {
                            where: {
                                sort: {
                                    [Op.gte]: data.endSort
                                },
                                workflowId: data.workflowId

                            }
                        })
                    }
                    else if (task.sort != data.endSort) {
                        let operator = '+';
                        let sortWhere = {};
                        if (data.endSort > task.sort) {
                            operator = '-';
                            sortWhere = {
                                [Op.gt]: task.sort,
                                [Op.lte]: data.endSort
                            };
                        }
                        else {
                            sortWhere = {
                                [Op.gte]: data.endSort,
                                [Op.lt]: task.sort
                            }
                        }


                        await self.db.Task.update({
                            sort: self.db.sequelize.literal(`sort${operator}1`)
                        }, {
                            where: {
                                sort: sortWhere,
                                workflowId: task.workflowId
                            }
                        })
                    }

                    task.workflowId = data.workflowId;
                    task.sort = data.endSort;
                    await task.save();
                }

                socket.broadcast.emit('task/move', data);
            });

            socket.on('task/create', async (data) => {

                let maxSort = await self.db.Task.findAll({
                    attributes: [[self.db.sequelize.fn('max', self.db.sequelize.col('sort')), 'maxSort']],
                    where: {
                        workflowId: data.workflowId
                    },
                    raw: true
                });

                data.sort = maxSort[0].maxSort + 1;


                
                data.creatorId = socket.user.id; 
                
                //write incoming message to database
                let task = self.db.Task.build();
                
                task.writeRemotes(data);

                console.log('data.sort',data.sort);

                try {
                    let result = await task.save();

                    let workflow = await self.db.Workflow.findOne({
                        where: {
                            id: task.workflowId
                        }
                    })

                    let user = await self.db.User.findOne({
                        where: {
                            id: task.assignedToId
                        }
                    })

                    let response = {
                        id: result.dataValues.id,
                        name: task.name,
                        text: task.text,
                        creator: task.creatorId,
                        assignedTo: user.fullname(),
                        project: task.projectId,
                        workflow: task.workflowId,
                        workflowcolor: workflow.color,
                        sort: data.sort
                    };

                    self.io.emit('task/create', response);
                }
                catch (err) {
                    console.log(err);
                    socket.emit('error', {
                        details: 'Could not save task, something went wrong',
                        task: data
                    })
                }



            })
        })
    }
}

module.exports = SocketHandler;