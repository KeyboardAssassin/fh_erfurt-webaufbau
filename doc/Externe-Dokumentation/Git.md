# Git

## Installation
* [Git herunterladen](https://git-scm.com/downloads) und installieren

## Einrichtung

Für die Nutzung von Git müssen Benutzername und E-Mail hinterlegt werden. Das wird später zur Authentifizierung am Git-Server genutzt. Folgende Kommandos mit eigenen Daten anpassen und mit der Git-Bash ausführen:
```sh
git config --global user.name "John Doe"
git config --global user.email john.doe@example.com
```

## Bestehendes Repository einrichten

Mit z.B. der Git-GUI das Repository klonen:
* Source: https://gitlab.com/jumble-x-sale/jumble-x-sale.git
* Target: ein lokales Verzeichnis das durch Git erst angelegt wird, also noch nicht existiere darf
* ![alt text](../Images/Git-Clone-1.png "Git: Klonen eines Repositories mit der Git-GUI, Teil 1")
* ![alt text](../Images/Git-Clone-2.png "Git: Klonen eines Repositories mit der Git-GUI, Teil 2")
